package com.sitthichai.bmi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.sitthichai.bmi.databinding.FragmentDescriptionBinding

class DescriptionFragment : Fragment() {
    private var _binding : FragmentDescriptionBinding? = null
    private val binding get() = _binding!!

    private lateinit var databases: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDescriptionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnClear.setOnClickListener {
            databases = FirebaseDatabase.getInstance().getReference("users")
            databases.removeValue()
            val action = DescriptionFragmentDirections.actionDescriptionFragmentToListFragment()
            view.findNavController().navigate(action)
        }
        binding.profile.setOnClickListener {
            val action = DescriptionFragmentDirections.actionDescriptionFragmentToListFragment()
            view.findNavController().navigate(action)
        }
    }
}