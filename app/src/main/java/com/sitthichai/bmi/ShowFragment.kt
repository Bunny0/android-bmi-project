package com.sitthichai.bmi

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.sitthichai.bmi.Model.User
import com.sitthichai.bmi.databinding.FragmentShowBinding
import java.util.*

class ShowFragment : Fragment() {
     private var _binding : FragmentShowBinding? = null
     private val binding get() = _binding!!

    var uniqueId = UUID.randomUUID().toString();

    private lateinit var result: String
    private lateinit var name: String
    private lateinit var date: String
    private lateinit var databases: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            result = it.getString(RESULT).toString()
            name = it.getString(NAME).toString()
            date = it.getString(DATE).toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShowBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.outputBmi.text = result
        binding.oupName.text = date
        if(result.toFloat() > 40) {
            binding.oupDes.text = "Obese Class III"
        }else if(result.toFloat() <= 40 && result.toFloat() > 35){
            binding.oupDes.text = "Obese Class II"
        }else if(result.toFloat() < 35 && result.toFloat() > 30){
            binding.oupDes.text = "Obese Class I"
        }else if(result.toFloat() < 30 && result.toFloat() > 25){
            binding.oupDes.text = "Overweight"
        }else if(result.toFloat() < 25 && result.toFloat() > 18.5){
            binding.oupDes.text = "Normal"
        }else if(result.toFloat() < 18.5 && result.toFloat() > 17){
            binding.oupDes.text = "Mild Thinness"
        }else if(result.toFloat() < 17 && result.toFloat() > 16){
            binding.oupDes.text = "Moderate Thinness"
        }else{
            binding.oupDes.text = "Severe Thinness"
        }




        binding.trashcan.setOnClickListener {
//            databases.child(uniqueId).removeValue()
            val action = ShowFragmentDirections.actionShowFragmentToListFragment()
            view.findNavController().navigate(action)
        }
        binding.download.setOnClickListener {
            databases = FirebaseDatabase.getInstance().getReference("users")
            val user = User(name,date,result.toFloat())
            databases.child(name).setValue(user)
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
            Log.d("Input Data :","$databases.toString()")
            val action = ShowFragmentDirections.actionShowFragmentToListFragment()
            view.findNavController().navigate(action)
        }

    }

    companion object{
        const val  RESULT = "result"
        const val  NAME = "name"
        const val  DATE = "date"
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}