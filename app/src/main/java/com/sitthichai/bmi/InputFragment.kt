package com.sitthichai.bmi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.sitthichai.bmi.databinding.FragmentInputBinding
import java.text.DateFormat
import java.util.*


class InputFragment : Fragment() {
    private var _binding : FragmentInputBinding? = null
    private val binding get() = _binding!!

    private var calendar: Calendar = Calendar.getInstance()
    private var currentDate: String = DateFormat.getDateInstance(DateFormat.FULL).format(calendar.time)
    val t = 0.0
    val bmi = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentInputBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.formatDate.text = currentDate
        binding.btnCancel.setOnClickListener {
            val action = InputFragmentDirections.actionInputFragmentToListFragment()
            view.findNavController().navigate(action)
        }
        binding.btnConfirm.setOnClickListener {
            var name = binding.inputName.text.toString()
            val height = binding.inputHeight.text.toString().toFloatOrNull()
            val weight = binding.inputWeight.text.toString().toFloatOrNull()
            var date = currentDate

            if (height != null && weight != null && name != "" && height.toString().isNotEmpty()
                && weight.toString().isNotEmpty() && weight.toString().toFloat() > 0.0.toFloat()
                && height.toString().toFloat() > 0.0.toFloat()) {
                val weightDiv = height / 100
                val weightTime2 = (weightDiv * weightDiv)
                val bmi = weight / weightTime2
                val result = bmi.toString()
                val action = InputFragmentDirections.actionInputFragmentToShowFragment(result,
                    name,date)
                view.findNavController().navigate(action)
            }else{
                //check error alter
                Toast.makeText(context, "Pleas Input Name, Height and Weight ", Toast.LENGTH_SHORT).show()
            }
        }
    }



    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}