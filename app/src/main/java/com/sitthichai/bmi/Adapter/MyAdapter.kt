package com.sitthichai.bmi.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.sitthichai.bmi.Model.User
import com.sitthichai.bmi.R


class MyAdapter : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {

    private val userList = ArrayList<User>()

    private lateinit var databases: DatabaseReference


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.fragment_item,
            parent,false)
            return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val currentItem = userList[position]

        holder.Name.text = currentItem.Name
        holder.Date.text = currentItem.Date
        holder.Bmi.text = currentItem.Bmi.toString()

        holder.itemView.setOnClickListener{
            Log.d("Test Onclick","Click Success")
            databases = FirebaseDatabase.getInstance().getReference("users").child(holder.Name.text.toString())
            databases.removeValue()
            Log.d("Test Delete : ","$holder.Name.text.toString()")
        }
        holder.itemView

    }

    override fun getItemCount(): Int {

        return userList.size
    }

    fun updateUserList(userList: List<User>){
        this.userList.clear()
        this.userList.addAll(userList)
        notifyDataSetChanged()
    }


    class  MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        val Name : TextView = itemView.findViewById(R.id.tvName)
        val Date : TextView = itemView.findViewById(R.id.tvDate)
        val Bmi : TextView = itemView.findViewById(R.id.tvBmi)

    }
}