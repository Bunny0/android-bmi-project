package com.sitthichai.bmi

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.DatabaseReference
import com.sitthichai.bmi.Adapter.MyAdapter
import com.sitthichai.bmi.Model.UserViewModel
import com.sitthichai.bmi.databinding.FragmentListBinding

private lateinit var viewModel : UserViewModel
private lateinit var userRecyclerView : RecyclerView
lateinit var adapter: MyAdapter

class ListFragment  : Fragment(){
    private var _binding : FragmentListBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userRecyclerView = view.findViewById(R.id.recycleView)
        userRecyclerView.layoutManager = LinearLayoutManager(context)
        userRecyclerView.setHasFixedSize(true)
        adapter = MyAdapter()
        userRecyclerView.adapter = adapter
        viewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        viewModel.allUsers.observe(viewLifecycleOwner, Observer{
            adapter.updateUserList(it)
        })


        binding.btnAdd.setOnClickListener {
             val action = ListFragmentDirections.actionListFragmentToInputFragment()
            view.findNavController().navigate(action)
        }
        binding.description.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToDescriptionFragment()
            view.findNavController().navigate(action)
        }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}